import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {
      Scanner input = new Scanner(System.in);

      System.out.println("Wie viele Mittelwerte sollen berechnet werden?");
      int runCount = input.nextInt();
      while (runCount > 0) {
         runCount --;
         System.out.println("Wie viele Mittelwerte sollen berechnet werden?");
         int numberCount = input.nextInt();
         int run = 0;
         double result = 0;
         while (run < numberCount) {
            numberCount ++;
            System.out.println("Geben Sie die " + run + ". Zahl an:");
            double number = input.nextDouble();

            result += number;
         }
         result = result/numberCount;

         System.out.printf("Der Mittelwert ist %.2f\n", result);
      }
   }
}
