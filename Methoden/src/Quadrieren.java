import java.util.Scanner;

public class Quadrieren {
   
	public static void main(String[] args) {
		titel();		
		
		double x = eingabe();
		
		double ergebnis = ergebnis(x);

		ausgabe(x, ergebnis);
	}	
	
	public static double ergebnis(double x) {
		double ergebnis = x * x;
		return ergebnis;
	}
	
	public static double eingabe() {
		Scanner input = new Scanner(System.in);
		System.out.println("Bitte eine Zahl eingeben: ");
		double x = input.nextDouble();
		
		return x;
	}
	
	
	public static void titel() {
		System.out.println("Dieses Programm berechnet die Quadratzahl x²");
		System.out.println("---------------------------------------------");
	}
	
	public static void ausgabe(double x, double ergebnis) {
		System.out.printf("x = %.2f und x²= %.2f\n", x, ergebnis);
	}
}
