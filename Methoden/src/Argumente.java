public class Argumente {
	public static void main(String[] args) {
		double value1 = 2.36;
		double value2 = 7.87;
		
		ausgabe(1, "Mana");
		ausgabe(2, "Elise");
		ausgabe(3, "Johanna");
		ausgabe(4, "Felizitas");
		ausgabe(5, "Karla");
		System.out.println(vergleichen(1, 2));
		System.out.println(vergleichen(1, 5));
		System.out.println(vergleichen(3, 4));
		
		System.out.println("\nAufgabe 2:");
		System.out.printf("%.2f * %.2f = %.4f", value1, value2, multiply(value1, value2));
	}

	public static void ausgabe(int zahl, String name) {
		System.out.println(zahl + ": " + name);
	}

	public static boolean vergleichen(int arg1, int arg2) {
		return (arg1 + 8) < (arg2 * 3);
	}
	
	public static double multiply(double value1, double value2) {
		return value1 * value2;
	}
}