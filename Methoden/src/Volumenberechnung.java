import java.util.Scanner;

public class Volumenberechnung {

    public static void main(String[] args) {
        double a = 2.0;
        double b = 2.0;
        double c = 2.0;
        double h = 2.0;
        double r = 2.0;

        wuerfel(a);
        quader(a, b, c);
        pyramide(a, h);
        kugel(r);
    }

    public static void wuerfel(double a) {
        System.out.printf("Würfel: V = a * a * a\nV = %f\n", a * a * a);
    }

    public static void quader(double a, double b, double c) {
        System.out.printf("Quader: V = a * b * c\nV = %f\n", a * b * c);
    }

    public static void pyramide(double a, double h) {
        System.out.printf("Pyramide: V = a * a * h / 3\nV = %f\n", a * a * h / 3);
    }

    public static void kugel(double r) {
        System.out.printf("Kugel: V = 4 / 3 * r³ * π\nV = %f", (4.0 / 3.0) * Math.pow(r, 3) * Math.PI);
    }

}
