import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class TicketHandler {

    /**
     * ArrayList containing all ticket names.
     * Their prices can be found in 'MoneyHandler.priceList'.
     */
    private final ArrayList<String> ticketList = new ArrayList<>(
            Arrays.asList(
                    "Einzelfahrschein Berlin AB",
                    "Einzelfahrschein Berlin BC",
                    "Einzelfahrschein Berlin ABC",
                    "Kurzstrecke",
                    "Tageskarte Berlin AB",
                    "Tageskarte Berlin BC",
                    "Tageskarte Berlin ABC",
                    "Kleingruppen-Tageskarte Berlin AB",
                    "Kleingruppen-Tageskarte Berlin BC",
                    "Kleingruppen-Tageskarte Berlin ABC"
            )
    );

    /**
     * The amount of tickets that can still be printed.
     */
    private Integer printableTickets = 100;

    /**
     * This is the customers order.
     * It's a map containing ticketIndex and amountTickets.
     * ticketIndex is the tickets index on ticketList.
     */
    private final HashMap<Integer, Integer> order = new HashMap<>();

    /**
     * A getter for the current total amount of tickets.
     */
    public Integer getTotalAmountTickets() {
        final Integer[] totalAmountTickets = {0};
        this.order.forEach((ticketType, amountTickets) -> totalAmountTickets[0] += amountTickets);

        return totalAmountTickets[0];
    }

    /**
     * A getter of the current ticketList.
     */
    public ArrayList<String> getTicketList() {
        return this.ticketList;
    }

    /**
     * A getter for the amount of printable tickets.
     */
    public Integer getPrintableTickets() {
        return this.printableTickets;
    }

    /**
     * A setter for the amount of printable tickets.
     */
    public void setPrintableTickets(Integer printableTickets) {
        this.printableTickets = printableTickets;
    }

    /**
     * A getter for the customers order
     */
    public HashMap<Integer, Integer> getOrder() {
        return order;
    }

    /**
     * Prints ALL of the orders tickets on the console.
     */
    public void printTickets(MoneyHandler moneyHandler) throws InterruptedException {
        System.out.print("\nFahrkarten werden gedruckt. Bitte warten.");
        TimeUnit.SECONDS.sleep(3);
        this.getOrder().forEach((ticketType, amountTickets) -> {
            this.printableTickets -= amountTickets;
            for (int i = amountTickets; i > 0; i--) {
                System.out.printf("""
                                                       
                                *  *  *  *  *  *  *  *  *  *  *  *  *  *
                                * %-36s *
                                * %-6.2f€                              *
                                *  *  *  *  *  *  *  *  *  *  *  *  *  *
                                """,
                        this.getTicketList().get(ticketType),
                        (double) (moneyHandler.getPriceList().get(ticketType)) / 100
                );
            }
        });
    }

    /**
     * Here the customers order is being taken.
     */
    public boolean takeOrder(Scanner input, MoneyHandler moneyHandler) {
        String vote;
        Integer amountTickets;
        this.initializeOrder(moneyHandler);
        do {
            this.printTicketList();
            Integer ticketIndex;

            while (true) {
                System.out.print("\nWähle ein Ticket aus der Liste: ");
                try {
                    ticketIndex = input.nextInt();
                } catch (InputMismatchException e) {
                    System.out.print("\nDas war keine korrekte Eingabe. Eingaben für Listeneinträge dürfen nur ganze Zahlen sein.\nBeispiel: 1");
                    input.nextLine();
                    continue;
                }

                break;
            }

            while (true) {
                System.out.print("\nAnzahl der Fahrkarten: ");
                try {
                    amountTickets = input.nextInt();
                } catch (InputMismatchException e) {
                    System.out.print("\nDas war keine korrekte Eingabe. Eingaben für Mengen dürfen nur ganze Zahlen sein.\nBeispiel: 1");
                    input.nextLine();
                    continue;
                }

                if (amountTickets < 1) {
                    System.out.print("\nSie können nicht weniger als 1 Fahrkarte bestellen.");
                }

                break;
            }

            if (this.getTotalAmountTickets() + amountTickets > this.printableTickets) {
                while (true) {
                    System.out.printf(
                            """

                                    Es können nur noch %d Fahrkarten gedruckt werden. Die aktuelle Gesamtzahl an Fahrkarten von %d kann nicht gedruckt werden.
                                    Wollen Sie eine andere Menge an Fahrkarten auswählen? (Ja/Nein)""",
                            this.printableTickets,
                            this.getTotalAmountTickets() + amountTickets
                    );

                    vote = input.next();
                    if (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Nein")) {
                        System.out.print("\nDas habe ich nicht verstanden.");
                        continue;
                    }

                    break;
                }

                if (vote.equals("Ja")) {
                    continue;
                }

                break;
            }

            this.order.put(
                    ticketIndex,
                    this.order.get(ticketIndex) + amountTickets
            );

            while (true) {
                System.out.printf(
                        "\nDer aktuelle Preis beträgt %.2f€ für %d Fahrkarten. Möchten Sie weitere Fahrkarten bestellen? (Ja/Nein/Abbrechen): ",
                        (double) (moneyHandler.getTotalPrice(this.order)) / 100,
                        this.getTotalAmountTickets()
                );

                vote = input.next();
                if (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Nein") && !Objects.equals(vote, "Abbrechen")) {
                    System.out.print("\nDas habe ich nicht verstanden.");
                    continue;
                }

                break;
            }
        } while (this.getTotalAmountTickets() < this.getPrintableTickets() && vote.equals("Ja"));

        if (vote.equals("Abbrechen")) {
            return true;
        }

        if (Objects.equals(this.getTotalAmountTickets(), this.printableTickets)) {
            System.out.printf(
                    "\nMaximale druckbare Ticket Anzahl von %d erreicht. Zu zahlender Gesamtpreis: %.2f",
                    this.printableTickets,
                    (double) (moneyHandler.getTotalPrice(this.order)) / 100
            );
        } else {
            System.out.printf(
                    "\nZu zahlender Gesamtpreis: %.2f",
                    (double) (moneyHandler.getTotalPrice(this.order)) / 100
            );
        }

        return false;
    }

    /**
     * Resets all values that are order specific
     */
    private void initializeOrder(MoneyHandler moneyHandler) {
        moneyHandler.initializeOrder();

        final Integer[] ticketIndex = {0};
        this.getTicketList().forEach(ticket -> {
            this.order.put(ticketIndex[0], 0);

            ticketIndex[0]++;
        });
    }

    /**
     * Prints the current ticketList.
     */
    public void printTicketList() {
        final Integer[] ticketIndex = {0};
        this.ticketList.forEach(ticketType -> {
            System.out.print("\n" + ticketIndex[0] + ": " + ticketType);
            ticketIndex[0]++;
        });
    }

    /**
     * This takes the users input on the administration menu and edits the specified ticket accordingly.
     */
    public void editTicket(Scanner input, MoneyHandler moneyHandler) {
        String ticketName;
        Integer ticketPrice;
        String vote = "";
        Integer[] ticketIndex = {0};

        do {
            ticketIndex[0] = 0;
            this.getTicketList().forEach(ticket -> {
                if (!Objects.equals(ticket, "")) {
                    System.out.print("\n" + ticketIndex[0] + ": " + ticket);
                    ticketIndex[0]++;
                }
            });

            ticketIndex[0] = 0;
            System.out.print("\nWähle eine Fahrkarte aus der Liste um es zu bearbeiten: ");
            if (getTicketIndex(input, ticketIndex)) continue;

            while (true) {
                System.out.printf(
                        "\nSoll die Fahrkarte '%s' bearbeitet werden? (Ja/Nein/Abbrechen) ",
                        this.getTicketList().get(ticketIndex[0])
                );
                vote = input.next();


                if (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Nein") && !Objects.equals(vote, "Abbrechen")) {
                    System.out.print("\nDas habe ich nicht verstanden.");
                    continue;
                }

                break;
            }

        } while (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Abbrechen"));

        if (vote.equals("Abbrechen")) {
            return;
        }

        ticketName = getTicketName(input);
        if (ticketName == null) return;

        ticketPrice = getTicketPrice(input);
        if (ticketPrice == null) return;

        System.out.printf(
                "\nFahrkarte erfolgreich bearbeitet: %s à %.2fEURO",
                ticketName,
                (double) (ticketPrice) / 100
        );
        this.getTicketList().set(ticketIndex[0], ticketName);
        moneyHandler.getPriceList().set(ticketIndex[0], ticketPrice);
    }

    /**
     * Waits for user input on the ticket index and validates it.
     * Used to specify which ticket should be edited/deleted
     */
    private boolean getTicketIndex(Scanner input, Integer[] ticketIndex) {
        try {
            ticketIndex[0] = input.nextInt();
        } catch (InputMismatchException e) {
            System.out.print("\nDas war keine korrekte Eingabe. Eingaben für Listeneinträge dürfen nur ganze Zahlen sein.\nBeispiel: 1");
            input.nextLine();
            return true;
        }
        try {
            this.getTicketList().get(ticketIndex[0]);
        } catch (IndexOutOfBoundsException e) {
            System.out.print("\nDiese Fahrkahrte gibt es nicht.");
            return true;
        }
        return false;
    }

    /**
     * Waits for user input on the ticket name and validates it.
     * Used for editing and adding new tickets.
     */
    public String getTicketName(Scanner input) {
        String ticketName;
        String vote = "";
        do {
            System.out.print("\nWie soll die Bezeichnung der Fahrkarte sein? ");
            ticketName = input.next();
            ticketName += input.nextLine();

            if (ticketName.length() < 5 || ticketName.length() > 36) {
                System.out.print("\nDie Bezeichnung einer Fahrkarte sollte wenigstens 5 und maximal 36 Zeichen lang sein.");
                continue;
            }

            while (true) {
                System.out.printf("\nSoll die Fahrkarte '%s' heißen? (Ja/Nein/Abbrechen) ", ticketName);
                vote = input.next();

                if (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Nein") && !Objects.equals(vote, "Abbrechen")) {
                    System.out.print("\nDas habe ich nicht verstanden.");
                    continue;
                }

                break;
            }
        } while (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Abbrechen"));

        if (vote.equals("Abbrechen")) {
            return null;
        }
        return ticketName;
    }

    /**
     * The ticket removal process.
     */
    public void removeTicket(Scanner input, MoneyHandler moneyHandler) {
        String vote = "";
        Integer[] ticketIndex = {0};
        do {
            ticketIndex[0] = 0;
            this.getTicketList().forEach(ticket -> {
                System.out.print("\n" + ticketIndex[0] + ": " + ticket);
                ticketIndex[0] = ticketIndex[0] + 1;
            });

            ticketIndex[0] = 0;
            System.out.print("\nWähle ein Ticket aus der Liste um es zu entfernen: ");
            if (getTicketIndex(input, ticketIndex)) continue;

            while (true) {
                System.out.printf(
                        "\nSoll die Fahrkarte '%s' entfernt werden? (Ja/Nein/Abbrechen) ",
                        this.getTicketList().get(ticketIndex[0])
                );
                vote = input.next();


                if (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Nein") && !Objects.equals(vote, "Abbrechen")) {
                    System.out.print("\nDas habe ich nicht verstanden.");
                    continue;
                }

                break;
            }
        } while (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Abbrechen"));

        if (vote.equals("Abbrechen")) {
            return;
        }

        System.out.printf(
                "\nFahrkarte erfolgreich entfernt: %s  à %.2fEURO",
                this.getTicketList().get(ticketIndex[0]),
                (double) (moneyHandler.getPriceList().get(ticketIndex[0])) / 100
        );
        this.getTicketList().remove((int) ticketIndex[0]);
        moneyHandler.getPriceList().remove(ticketIndex[0]);
    }

    /**
     * The ticket creation process.
     */
    public void addTicket(Scanner input, MoneyHandler moneyHandler) {
        String ticketName;
        Integer ticketPrice;

        ticketName = this.getTicketName(input);
        if (ticketName == null) return;

        ticketPrice = this.getTicketPrice(input);
        if (ticketPrice == null) return;

        System.out.printf(
                "\nFahrkarte erfolgreich hinzugefügt: %s à %.2fEURO",
                ticketName,
                (double) (ticketPrice) / 100
        );
        this.getTicketList().add(ticketName);
        moneyHandler.getPriceList().add(ticketPrice);
    }

    /**
     * Waits for user input on the ticket price and validates it.
     * Used for editing and adding new tickets.
     */
    public Integer getTicketPrice(Scanner input) {
        Integer ticketPrice;
        String vote = "";
        do {
            ticketPrice = 0;
            System.out.print("\nWie viel soll die Fahrkarte kosten? (EURO) ");
            try {
                ticketPrice = (int) (input.nextDouble() * 100);
            } catch (InputMismatchException e) {
                System.out.print("\nDas war keine korrekte Eingabe. Eingaben für Geld müssen im Format EURO.CENT geschehen.\nBeispiel: 1.0 oder 1");
                input.nextLine();
                continue;
            }

            if (ticketPrice <= 0) {
                System.out.print("\nWir haben nichts zu verschenken. Der Fahrkartenpreis kann nicht kleiner oder gleich 0 sein.");
                continue;
            }

            while (true) {
                System.out.printf(
                        "\nSoll die Fahrkarte %.2fEURO kosten? (Ja/Nein/Abbrechen) ",
                        (double) (ticketPrice) / 100
                );
                vote = input.next();


                if (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Nein") && !Objects.equals(vote, "Abbrechen")) {
                    System.out.print("\nDas habe ich nicht verstanden.");
                    continue;
                }

                break;
            }
        } while (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Abbrechen"));

        if (vote.equals("Abbrechen")) {
            return null;
        }

        return ticketPrice;
    }

    /**
     * This prints the customers receipt after the tickets where printed.
     */
    public void printReceipt(MoneyHandler moneyHandler) throws InterruptedException {
        System.out.print("\nBeleg wird gedruckt. Bitte warten.");
        TimeUnit.SECONDS.sleep(3);

        System.out.printf(
                """
                                               
                        *  *  *  *  *  *  *  *  *  *  *  *  *  *
                        *             Rechnungsbeleg           *
                        *         %-28s *
                        *                                      *""",
                new SimpleDateFormat("dd.MM.yyyy 'um' HH:mm:ss")
                        .format(new Date(System.currentTimeMillis()))
        );

        this.getOrder().forEach((ticketType, amountTickets) -> {
            if (amountTickets > 0) {
                System.out.printf(
                        """
                                                
                                * %-36s *
                                * %-4d à %-6.2f EURO                   *
                                *                                      *""",
                        this.getTicketList().get(ticketType),
                        amountTickets,
                        (double) (moneyHandler.getPriceList().get(ticketType)) / 100);
            }
        });

        System.out.printf(
                """
                                                
                        * Gesamtpreis: %-8.2f EURO           *
                        *                                      *
                        *  *  *  *  *  *  *  *  *  *  *  *  *  *
                                        
                        """,
                (double) (moneyHandler.getTotalPrice(this.order)) / 100
        );
    }
}
