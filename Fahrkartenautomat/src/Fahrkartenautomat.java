import java.util.Scanner;

class Fahrkartenautomat {
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;

		zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

		// Geldeinwurf
		// -----------
		eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag, tastatur);

		// Fahrscheinausgabe
		// -----------------
		fahrkartenAusgeben();

		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);

		System.out.println("""

				Vergessen Sie nicht, den Fahrschein
				vor Fahrtantritt entwerten zu lassen!
				Wir wünschen Ihnen eine gute Fahrt.""");
		/* 5. Datentyp: int
		 * 	  Begründung: Ich bin PHP Dev. Ich achte nicht auf Dinge wie “byte” oder “short”. Sowas interessiert mich, wenn überhaupt nur, in MySQL. 
		 * 
		 * 6. Die beiden Werte (int und double) werden multipliziert. 
		 */
	}

	public static double fahrkartenbestellungErfassen(Scanner tastatur) {
		System.out.print("Anzahl der Tickets: ");
		int ticketCount = tastatur.nextInt();
		if (ticketCount > 10) {
			System.out.print("Druckertinte reicht nicht für mehr als 10 Tickets. Ticketanzahl wird auf 1 gesetzt. Bitte zeitnah neue Patrone einlegen.\n");
			ticketCount = 1;
		} else if (ticketCount < 1) {
			System.out.print("Nicht immer so negativ denken. Ticketanzahl wird auf 1 gesetzt.\n");
			ticketCount = 1;
		}
		System.out.print("Ticketpreis (EURO): ");
		double  ticketPrice = tastatur.nextDouble();

		if (ticketPrice < 0) {
			ticketPrice = 1;
			System.out.print("Wir haben kein Geld zu verschenken. Ticketpreis auf 1€ festgelegt.\n");
		}

		return ticketCount * ticketPrice;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
		double eingezahlterGesamtbetrag = 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f EURO\n", zuZahlenderBetrag - eingezahlterGesamtbetrag);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			double eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}

		return eingezahlterGesamtbetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");

		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(250);
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (rueckgabebetrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rueckgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
			{
				muenzeAusgeben(2, "EURO");
				rueckgabebetrag -= 2.0;
			}
			while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
			{
				muenzeAusgeben(1, "EURO");
				rueckgabebetrag -= 1.0;
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabebetrag -= 0.5;
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
			{
				muenzeAusgeben(20, "CENT");
				rueckgabebetrag -= 0.2;
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
			{
				muenzeAusgeben(10, "CENT");
				rueckgabebetrag -= 0.1;
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
			{
				muenzeAusgeben(5, "CENT");
				rueckgabebetrag -= 0.05;
			}
		}
	}

	public static void warte(int duration) {
		try {
			Thread.sleep(duration);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + einheit);
	}
}