import java.util.*;
import java.util.concurrent.TimeUnit;

class RefactoredTicketMachine {
    /**
     * Scanner used for user inputs.
     */
    private static final Scanner input = new Scanner(System.in);

    /**
     * TicketHandler that handles all the ticket related stuff.
     */
    private static final TicketHandler ticketHandler = new TicketHandler();

    /**
     * MoneyHandler that handles all the money related stuff.
     */
    private static final MoneyHandler moneyHandler = new MoneyHandler();

    /**
     * Boolean variable that indicates if the system should shut down.
     * Used to stop the program.
     */
    private static boolean isShutDown = false;

    /**
     * Menu points of the main menu.
     */
    private static final ArrayList<String> menuPointList = new ArrayList<>(Arrays.asList(
            "Fahrkartenverkauf",
            "Administration"
    ));

    /**
     * Menu points of the administration menu.
     */
    private static final ArrayList<String> adminMenuPointList = new ArrayList<>(Arrays.asList(
            "Kasse",
            "Fahrkarten",
            "Herunterfahren",
            "Zurück"
    ));

    /**
     * Menu points of the register specific administration menu.
     */
    private static final ArrayList<String> adminMenuRegisterPointList = new ArrayList<>(Arrays.asList(
            "Kasse auffüllen",
            "Kasse leeren",
            "Zurück"
    ));

    /**
     * Menu points of the ticket specific administration menu.
     */
    private static final ArrayList<String> adminMenuTicketPointList = new ArrayList<>(Arrays.asList(
            "Fahrkarte hinzufügen",
            "Fahrkarte entfernen",
            "Fahrkarte bearbeiten",
            "Fahrkartenrohlinge auffüllen",
            "Zurück"
    ));

    public static void main(String[] args) throws InterruptedException {
        do {
            // Main menu
            String action = showMenu();

            if (Objects.equals(action, menuPointList.get(0))) {
                // Ticket shop
                boolean shouldCancel = ticketHandler.takeOrder(input, moneyHandler);
                if (shouldCancel) {
                    continue;
                }

                do {
                    moneyHandler.processPayment(input, ticketHandler);
                } while (!moneyHandler.processChange(ticketHandler));

                ticketHandler.printTickets(moneyHandler);
                ticketHandler.printReceipt(moneyHandler);
            } else if (Objects.equals(action, menuPointList.get(1))) {
                // Admin menu
                showAdminMenu();
            } else {
                System.out.print("\nDiese Aktion gibt es nicht.");
            }
        } while (!isShutDown);
    }

    /**
     * This is basically the admin menu.
     */
    private static void showAdminMenu() throws InterruptedException {
        Integer action;
        do {
            System.out.print("\nAdministration");
            action = getNextAction(adminMenuPointList);
            if (action == null) continue;

            if (Objects.equals(adminMenuPointList.get(action), adminMenuPointList.get(0))) {
                showAdminRegisterMenu();
            } else if (Objects.equals(adminMenuPointList.get(action), adminMenuPointList.get(1))) {
                showAdminTicketMenu();
            } else if (Objects.equals(adminMenuPointList.get(action), adminMenuPointList.get(2))) {
                isShutDown = true;
                System.out.print("\nSystem fährt herunter. Bitte warten.");
                TimeUnit.SECONDS.sleep(3);
                break;
            } else if (Objects.equals(adminMenuPointList.get(action), adminMenuPointList.get(3))) {
                break;
            }
        } while (true);
    }

    /**
     * This lists all menuPoints and waits for the users input. Also handles wrong inputs.
     */
    private static Integer getNextAction(ArrayList<String> menuPointList) {
        Integer action;
        listMenuPoints(menuPointList);

        System.out.print("\nWähle eine Aktion: ");
        try {
            action = input.nextInt();
        } catch (InputMismatchException e) {
            System.out.print("\nDas war keine korrekte Eingabe. Eingaben für Listeneinträge dürfen nur ganze Zahlen sein.\nBeispiel: 1");
            input.nextLine();
            return null;
        }

        try {
            menuPointList.get(action);
        } catch (IndexOutOfBoundsException e) {
            System.out.print("\nDiese Aktion gibt es nicht.");
            return null;
        }
        return action;
    }

    /**
     * This is the ticket specific administration menu.
     */
    private static void showAdminTicketMenu() {
        final Integer[] ticketIndex = {0};
        do {
            ticketIndex[0] = 0;
            System.out.print("\nAktuelle Fahrkarten");
            ticketHandler.getTicketList().forEach(ticket -> {
                System.out.printf(
                        "\n%s à %.2fEURO",
                        ticket,
                        (double) (moneyHandler.getPriceList().get(ticketIndex[0])) / 100
                );

                ticketIndex[0]++;
            });

            System.out.print("\n");
            Integer action = getNextAction(adminMenuTicketPointList);
            if (action == null) continue;

            if (Objects.equals(adminMenuTicketPointList.get(action), adminMenuTicketPointList.get(0))) {
                ticketHandler.addTicket(input, moneyHandler);
            } else if (Objects.equals(adminMenuTicketPointList.get(action), adminMenuTicketPointList.get(1))) {
                ticketHandler.removeTicket(input, moneyHandler);
            } else if (Objects.equals(adminMenuTicketPointList.get(action), adminMenuTicketPointList.get(2))) {
                ticketHandler.editTicket(input, moneyHandler);
            } else if (Objects.equals(adminMenuTicketPointList.get(action), adminMenuTicketPointList.get(3))) {
                while (true) {
                    System.out.print("\nWie viele Fahrkartenrohlinge möchten Sie hinzufügen? ");
                    try {
                        ticketHandler.setPrintableTickets(ticketHandler.getPrintableTickets() + input.nextInt());
                    } catch (InputMismatchException e) {
                        System.out.print("\nDas war keine korrekte Eingabe. Eingaben für Mengen dürfen nur ganze Zahlen sein.\nBeispiel: 1");
                        input.nextLine();
                        continue;
                    }
                    System.out.print(
                            "\nFahrkartenrohlinge erfolgreich aufgefüllt. Aktueller Stand: "
                                    + ticketHandler.getPrintableTickets()
                    );
                    break;
                }
            } else if (Objects.equals(adminMenuTicketPointList.get(action), adminMenuTicketPointList.get(4))) {
                break;
            }
        } while (true);
    }

    /**
     * This is the register specific administration menu.
     */
    private static void showAdminRegisterMenu() {
        final Integer[] coinIndex = {0};
        do {
            coinIndex[0] = 0;
            System.out.print("\nAktueller Kassenstand");
            moneyHandler.getRegister().forEach(amount -> {
                System.out.printf(
                        "\n%d%s: %d Stück",
                        moneyHandler.getAvailableCoins().get(coinIndex[0]) >= 100
                                ? moneyHandler.getAvailableCoins().get(coinIndex[0]) / 100
                                : moneyHandler.getAvailableCoins().get(coinIndex[0]),
                        moneyHandler.getAvailableCoins().get(coinIndex[0]) >= 100 ? "EURO" : "CENT",
                        amount
                );

                coinIndex[0]++;
            });

            System.out.print("\n");
            Integer action = getNextAction(adminMenuRegisterPointList);
            if (action == null) continue;

            if (Objects.equals(adminMenuRegisterPointList.get(action), adminMenuRegisterPointList.get(0))) {
                moneyHandler.fillRegister(input);
            } else if (Objects.equals(adminMenuRegisterPointList.get(action), adminMenuRegisterPointList.get(1))) {
                moneyHandler.clearRegister(input);
            } else if (Objects.equals(adminMenuRegisterPointList.get(action), adminMenuRegisterPointList.get(2))) {
                break;
            }
        } while (true);
    }

    /**
     * Loops through the given ArrayList and prints all menuPoints.
     */
    private static void listMenuPoints(ArrayList<String> menuPointList) {
        final Integer[] menuPointIndex = {0};
        menuPointList.forEach(menuPoint -> {
            System.out.print("\n" + menuPointIndex[0] + ": " + menuPoint);
            menuPointIndex[0]++;
        });
    }

    /**
     * The main menu.
     */
    private static String showMenu() {
        Integer action;
        while (true) {
            listMenuPoints(menuPointList);

            System.out.print("\nWähle eine Aktion: ");
            try {
                action = input.nextInt();
            } catch (InputMismatchException e) {
                System.out.print("\nDas war keine korrekte Eingabe. Eingaben für Listeneinträge dürfen nur ganze Zahlen sein.\nBeispiel: 1\n");
                input.nextLine();
                continue;
            }

            break;
        }

        try {
            menuPointList.get(action);
        } catch (IndexOutOfBoundsException e) {
            return "";
        }

        return menuPointList.get(action);
    }
}