import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

import static java.util.Collections.reverse;

public class MoneyHandler {
    /** This is the list of coins used as payment. */
    private final ArrayList<Integer> paidCoinList = new ArrayList<>();

    /** This is the list of coins used as change. */
    private final ArrayList<Integer> changeCoinList = new ArrayList<>();

    /** This is the price list, listing prices for all tickets. */
    private final ArrayList<Integer> priceList = new ArrayList<>(
            Arrays.asList(290, 330, 360, 190, 860, 900, 960, 2350, 2430, 2490)
    );

    /**
     * This is the cash register.
     * An array including the amount of specific coins and bills.
     * Index: Coin/Bill
     * 0: 0.05€, 1: 0.10€, 2: 0.20€, 3: 0.50€, 4: 1€, 5: 2€, 6: 5€, 7: 10€, 8: 20€, 8: 50€, 9: 100€
     */
    private final ArrayList<Integer> register = new ArrayList<>(
            Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)
    );

    /**
     * This is an array of all coins and bills that are being accepted by the ticket machine.
     * It also perfectly fits the register variable.
     * In order to get the actual value you have to divide by 100.
     */
    private final ArrayList<Integer> availableCoins = new ArrayList<>(
            Arrays.asList(5, 10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000)
    );

    /** A getter for the priceList ArrayList */
    public ArrayList<Integer> getPriceList() {
        return this.priceList;
    }

    /** A getter for the register ArrayList */
    public ArrayList<Integer> getRegister() {
        return this.register;
    }

    /** A getter for the availableCoins ArrayList */
    public ArrayList<Integer> getAvailableCoins() {
        return this.availableCoins;
    }

    /** A getter for the current amount of money that was already paid */
    private Integer getAmountPaid() {
        final Integer[] amountPaid = {0};
        this.paidCoinList.forEach(coin -> amountPaid[0] += coin);

        return amountPaid[0];
    }

    /** A getter for the current total price. */
    public Integer getTotalPrice(HashMap<Integer, Integer> order) {
        AtomicReference<Integer> totalPrice = new AtomicReference<>(0);
        order.forEach((ticketType, amountTickets) -> totalPrice.updateAndGet(v -> v + this.priceList.get(ticketType) * amountTickets));
        return totalPrice.get();
    }

    public void processPayment(Scanner input, TicketHandler ticketHandler) {
        Integer totalPrice = this.getTotalPrice(ticketHandler.getOrder());
        Integer coin;
        do {
            boolean correctInput = false;

            System.out.print("\nBitte Geld einwerfen: ");
            try {
                coin = (int)(input.nextDouble() * 100);
            } catch (InputMismatchException e) {
                System.out.print("\nDas war keine korrekte Eingabe. Eingaben für Geld müssen im Format EURO.CENT geschehen.\nBeispiel: 1.0 oder 1");
                input.nextLine();
                continue;
            }

            Integer coinIndex = 0;
            for (Integer availableCoin : this.availableCoins) {
                if (Objects.equals(availableCoin, coin)) {
                    correctInput = true;

                    break;
                }

                coinIndex++;
            }

            if (correctInput) {
                this.register.set(coinIndex, this.register.get(coinIndex) + 1);
                this.paidCoinList.add(coin);
            } else {
                System.out.print("\nDieses Geldstück wird nicht unterstützt.\nDie folgenden Münzen und Scheine werden unterstützt:");
                this.availableCoins.forEach(availableCoin -> System.out.printf("\n%.2fEURO", (double)(availableCoin) / 100));
            }

            if (totalPrice > this.getAmountPaid()) {
                System.out.printf("\nNoch zu zahlender Betrag: %.2fEURO", (double)(totalPrice - this.getAmountPaid()) / 100);
            }
        } while (totalPrice > this.getAmountPaid());
    }

    public Boolean processChange(TicketHandler ticketHandler) {
        if (!this.isChangeNeeded(ticketHandler)) {
            return true;
        }

        Integer change = this.getAmountPaid() - this.getTotalPrice(ticketHandler.getOrder());

        Boolean changeAvailable;
        changeAvailable = isChangeAvailable(change);
        if (!changeAvailable) {
            this.changeCoinList.forEach(coin -> {
                int coinIndex = this.availableCoins.indexOf(coin);
                this.register.set(coinIndex, this.register.get(coinIndex) + 1);
            });
            this.changeCoinList.clear();

            this.paidCoinList.forEach(coin -> {
                int coinIndex = this.availableCoins.indexOf(coin);
                this.register.set(coinIndex, this.register.get(coinIndex) - 1);
            });
            this.paidCoinList.clear();

            System.out.print("\nDer Betrag kann leider nicht gewechselt werden. Entnehmen Sie bitte Ihr Geld aus der Klappe und zahlen Sie passend.");
            return false;
        }

        if (change >= 100) {
            System.out.printf("\nSie bekommen %.2fEURO Rückgeld.", (double)(change) / 100);
        } else {
            System.out.printf("\nSie bekommen %dCENT Rückgeld.", change);
        }

        printCoinsAndBills();
        return true;
    }

    /** Returns true or false depending on if change is needed */
    public boolean isChangeNeeded(TicketHandler ticketHandler) {
        return this.getTotalPrice(ticketHandler.getOrder()) < this.getAmountPaid();
    }

    /**
     * Checks if change can be returned.
     * Loops through all available coins and checks if they are available in the register
     * Returns true if change can be returned. Returns false otherwise.
     */
    private boolean isChangeAvailable(Integer change) {
        ArrayList<Integer> availableCoins = this.availableCoins;
        reverse(availableCoins);

        do {
            Integer coinIndex = availableCoins.size();
            for (Integer availableCoin : availableCoins) {
                coinIndex--;

                if (availableCoin <= change) {
                    if (this.register.get(coinIndex) > 0) {
                        change -= availableCoin;
                        this.register.set(coinIndex, this.register.get(coinIndex) - 1);
                        this.changeCoinList.add(availableCoin);

                        break;
                    }
                }
            }

            if (coinIndex == 0 && this.register.get(coinIndex) == 0 && change >= 5) {
                return false;
            }
        } while (change >= 5);

        return true;
    }


    /** Prints Coins and Bills that are being returned as change. */
    public void printCoinsAndBills() {
        this.changeCoinList.forEach(coin -> {
            if(coin >= 500) {
                System.out.printf("""
                                               
                        *  *  *  *  *  *  *  *  *  *  *  *  *  *
                        *                                      *
                        *               %3d EURO               *
                        *                                      *
                        *  *  *  *  *  *  *  *  *  *  *  *  *  *
                         """,
                        coin / 100
                );
            } else {
                System.out.printf("""
                                    
                                    * * *
                                 *         *
                                *     %3d   *
                                *    %-4s   *
                                 *         *
                                    * * *
                                """,
                        coin >= 100 ? coin / 100 : coin,
                        coin >= 100 ? "EURO" : "CENT"
                );
            }
        });
    }

    /** Empties the register if the user said so on the administration menu */
    public void clearRegister(Scanner input) {
        String vote;
        while (true) {
            System.out.print("\nWollen sie die Kasse wirklich entleeren? (Ja/Nein) ");
            vote = input.next();


            if (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Nein")) {
                System.out.print("\nDas habe ich nicht verstanden.");
                continue;
            }

            break;
        }

        if (vote.equals("Nein")) {
            return;
        }

        this.getRegister().replaceAll(amount -> amount = 0);
    }

    /** Adds coins and bills to the register depending on the users input */
    public void fillRegister(Scanner input) {
        final Integer[] coinIndex = {0};
        Integer amount;
        String vote;
        do {
            coinIndex[0] = 0;
            this.getAvailableCoins().forEach(coin -> {
                System.out.print("\n" + coinIndex[0] + ": " + (coin >= 100 ? coin / 100 + "EURO" : coin + "CENT"));
                coinIndex[0]++;
            });

            while (true) {
                coinIndex[0] = 0;
                System.out.print("\nWelche der aufgelisteten Münzen/Scheine möchten Sie auffüllen? ");
                try {
                    coinIndex[0] = input.nextInt();
                } catch (InputMismatchException e) {
                    System.out.print("\nDas war keine korrekte Eingabe. Eingaben für Listeneinträge dürfen nur ganze Zahlen sein.\nBeispiel: 1");
                    input.nextLine();
                    continue;
                }

                if (coinIndex[0] <= 10) {
                    break;
                }

                System.out.print("\nSie können nur die aufgelisteten Fächer auffüllen");
            }

            while (true) {
                System.out.print("\nWie viele Münzen/Scheine möchten Sie auffüllen? ");
                try {
                    amount = input.nextInt();
                } catch (InputMismatchException e) {
                    System.out.print("\nDas war keine korrekte Eingabe. Eingaben für Mengen dürfen nur ganze Zahlen sein.\nBeispiel: 1");
                    input.nextLine();
                    continue;
                }

                if (amount > 0) {
                    break;
                }

                System.out.print("\nSie können nur 1 oder mehr Münzen/Scheine auffüllen");
            }

            this.getRegister().set(coinIndex[0], this.getRegister().get(coinIndex[0]) + amount);
            System.out.printf(
                    "\nKasse erfolgreich aufgefüllt. Es sind befinden sich nun %d %d%s in der Kasse.",
                    this.getRegister().get(coinIndex[0]),
                    this.getAvailableCoins().get(coinIndex[0]) >= 100
                            ? this.getAvailableCoins().get(coinIndex[0]) / 100
                            : this.getAvailableCoins().get(coinIndex[0]),
                    this.getAvailableCoins().get(coinIndex[0]) >= 100 ? "EURO" : "CENT"
            );

            while (true) {
                System.out.print("\nWollen sie weitere Kassenfächer auffüllen? (Ja/Nein) ");
                vote = input.next();


                if (!Objects.equals(vote, "Ja") && !Objects.equals(vote, "Nein")) {
                    System.out.print("\nDas habe ich nicht verstanden.");
                    continue;
                }

                break;
            }
        } while (vote.equals("Ja"));
    }

    /** Resets all values that are order specific. */
    public void initializeOrder() {
        this.changeCoinList.clear();
        this.paidCoinList.clear();
    }
}
