import java.util.Scanner;

public class Greeter {

	public static void main(String[] args) {
		Scanner Scanner = new Scanner(System.in);

		System.out.print("Greetings, allmighty user. May you tell me your name?");
		String name = Scanner.next();
		System.out.print("While we're at it, can you also remind me of your age? Why? Oh no reason.");
		int age = Scanner.nextInt();
		System.out.printf("Name: %s\nAge: %d", name, age);
	}

}
