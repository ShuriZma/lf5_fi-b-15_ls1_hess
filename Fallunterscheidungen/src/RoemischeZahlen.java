import java.util.Scanner;

public class RoemischeZahlen {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Geben Sie eine Zahl an.");
        char romanNumber = input.next().charAt(0);

        switch (romanNumber) {
            case 'I' -> System.out.print("1");
            case 'V' -> System.out.print("5");
            case 'X' -> System.out.print("10");
            case 'L' -> System.out.print("50");
            case 'C' -> System.out.print("100");
            case 'D' -> System.out.print("500");
            case 'M' -> System.out.print("1000");
            default -> System.out.printf("%c ist keine gültige römische Zahl.", romanNumber);
        }
    }
}
