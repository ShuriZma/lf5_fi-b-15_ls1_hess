import java.util.Scanner;

public class OhmschesGesetz {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Welche Größe möchtest du berechnen (R,U,I).");
        char quantity = input.next().charAt(0);
        float[] values;
        switch (quantity) {
            case 'R' -> {
                values = getValues(input, quantity);
                System.out.printf("R = %.2f / %.2f = %.2f", values[0], values[1], values[0] / values[1]);
            }
            case 'U' -> {
                values = getValues(input, quantity);
                System.out.printf("U = %.2f * %.2f = %.2f", values[0], values[1], values[0] * values[1]);
            }
            case 'I' -> {
                values = getValues(input, quantity);
                System.out.printf("I = %.2f * %.2f = %.2f", values[0], values[1], values[0] * values[1]);
            }
            default -> System.out.print("Die angegebene physikalische Größe ist mir unbekannt.");
        }
    }

    private static float[] getValues(Scanner input, char quantity) {
        float valueForU;
        float valueForI;
        float valueForR;

        switch (quantity) {
            case 'R' -> {
                System.out.print("Geben Sie den Wert für U an.");
                valueForU = input.nextFloat();
                System.out.print("Geben Sie den Wert für I an.");
                valueForI = input.nextFloat();
                return new float[]{valueForU, valueForI};
            }
            case 'U' -> {
                System.out.print("Geben Sie den Wert für U an.");
                valueForR = input.nextFloat();
                System.out.print("Geben Sie den Wert für I an.");
                valueForI = input.nextFloat();
                return new float[]{valueForR, valueForI};
            }
            case 'I' -> {
                System.out.print("Geben Sie den Wert für U an.");
                valueForU = input.nextFloat();
                System.out.print("Geben Sie den Wert für I an.");
                valueForR = input.nextFloat();
                return new float[]{valueForU, valueForR};
            }
        }
        return new float[]{};
    }
}
