import java.util.Scanner;

public class Noten {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Geben Sie eine Zahl an.");
        int grade = input.nextInt();

        switch (grade) {
            case 1 -> System.out.print("Sehr gut!");
            case 2 -> System.out.print("Gut!");
            case 3 -> System.out.print("Befriedigend!");
            case 4 -> System.out.print("Ausreichend!");
            case 5 -> System.out.print("Mangelhaft!");
            case 6 -> System.out.print("Ungenügend!");
            default -> System.out.print("Bei Ihrer Eingabe handelt es sich nicht um eine Note unseres Notensystems (1-6).");
        }
    }
}
