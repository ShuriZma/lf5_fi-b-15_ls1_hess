import java.util.Scanner;

public class Taschenrechner {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Geben Sie die erste Zahl an.");
        float firstNumber = input.nextFloat();
        System.out.print("Geben Sie die zweite Zahl an.");
        float secondNumber = input.nextFloat();
        System.out.print("Geben Sie an welcher mathematische Operator verwendet werden soll (+,-,*,/).");
        char operator = input.next().charAt(0);

        switch (operator) {
            case '+' -> System.out.printf("%.2f + %.2f = %.2f", firstNumber, secondNumber, firstNumber + secondNumber);
            case '-' -> System.out.printf("%.2f + %.2f = %.2f", firstNumber, secondNumber, firstNumber - secondNumber);
            case '*' -> System.out.printf("%.2f + %.2f = %.2f", firstNumber, secondNumber, firstNumber * secondNumber);
            case '/' -> System.out.printf("%.2f + %.2f = %.2f", firstNumber, secondNumber, firstNumber / secondNumber);
            default -> System.out.print("Ups! Der angegebene Operator ist ungeültig! Damit hab ich nicht gerechnet! (Pun intended)");
        }

    }
}
