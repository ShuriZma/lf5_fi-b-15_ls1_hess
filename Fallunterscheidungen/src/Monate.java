import java.util.Scanner;

public class Monate {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Geben Sie eine Zahl von 1-12 an.");
        int month = input.nextInt();

        switch (month) {
            case 1 -> System.out.print("Januar.");
            case 2 -> System.out.print("Februar.");
            case 3 -> System.out.print("März");
            case 4 -> System.out.print("April.");
            case 5 -> System.out.print("Mai.");
            case 6 -> System.out.print("Juni.");
            case 7 -> System.out.print("Juli.");
            case 8 -> System.out.print("August.");
            case 9 -> System.out.print("September.");
            case 10 -> System.out.print("Oktober.");
            case 11 -> System.out.print("November.");
            case 12 -> System.out.print("Dezember.");
            default -> System.out.print("Laut unserem Kalendar hat Jahr hat nur 12 Monate. Sorry.");
        }
    }
}
