/**
 * Class {@link Captain}
 * @author Markus Heß / ShuriZma
 */
public class Captain {
    /** Name of the {@link Captain}. */
    private String name = "";
    /** Since when is the {@link Captain} a {@link Captain}. */
    private int captainSince = 0;
    /** Which {@link Spaceship} is the {@link Captain}, the {@link Captain} of. */
    private String captainOf = "";

    /** Default constructor */
    public Captain() {}

    /**
     * Full constructor
     * @param name {@link Captain#name}
     * @param captainSince {@link Captain#captainSince}
     * @param captainOf {@link Captain#captainOf}
     */
    public Captain(String name, int captainSince, String captainOf) {
        this.name = name;
        this.captainSince = captainSince;
        this.captainOf = captainOf;
    }

    /** @return {@link Captain#name} */
    public String getName() {
        return name;
    }

    /** @param name {@link Captain#name} */
    public void setName(String name) {
        this.name = name;
    }

    /** @return {@link Captain#captainSince} */
    public int getCaptainSince() {
        return captainSince;
    }

    /** @param captainSince {@link Captain#captainSince} */
    public void setCaptainSince(int captainSince) {
        this.captainSince = captainSince;
    }

    /** @return {@link Captain#captainOf} */
    public String getCaptainOf() {
        return captainOf;
    }

    /** @param captainOf {@link Captain#captainOf} */
    public void setCaptainOf(String captainOf) {
        this.captainOf = captainOf;
    }
}
