/**
 * Class {@link Cargo}
 * @author Markus Heß / ShuriZma
 */
public class Cargo {
    /** Name of the {@link Cargo} */
    private String name = "";
    /** Amount of the {@link Cargo} */
    private int amount = 0;

    /** Default constructor */
    public Cargo(){}

    /**
     * @param name {@link Cargo#name}
     * @param amount {@link Cargo#amount}
     */
    public Cargo(String name, int amount) {
        this.name = name;
        this.amount = amount;
    }

    /** @return {@link Cargo#name} */
    public String getName() {
        return name;
    }

    /** @param name {@link Cargo#name} */
    public void setName(String name) {
        this.name = name;
    }

    /** @return {@link Cargo#amount} */
    public int getAmount() {
        return amount;
    }

    /** @param amount {@link Cargo#amount} */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /** @return {@link Cargo} as {@link String} */
    @Override
    public String toString() {
        return this.getClass().getName() + ", Name: " + this.name + ", Amount: " + this.amount + "\n\n";
    }
}
