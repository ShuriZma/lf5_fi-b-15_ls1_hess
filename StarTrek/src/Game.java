import java.util.ArrayList;
import java.util.Arrays;

/**
 * Class {@link Game}
 * @author Markus Heß / ShuriZma
 *
 * Runs the actual game and utilizes the other classes.
 */
public class Game {

    /** List of {@link Spaceship} in the game */
    private static ArrayList<Spaceship> spaceships = new ArrayList<>();

    public static void main(String[] args) {
        Spaceship klingonen = new Spaceship(
                "IKS Hegh'ta",
                100.0,
                100.0,
                100.0,
                100.0,
                1,
                2,
                new ArrayList<>(
                        Arrays.asList(
                                new Cargo("Ferengi Schneckensaft", 200),
                                new Cargo("Bat'leth Klingonen Schwert", 200)
                        )
                )
        );

        Spaceship romulaner = new Spaceship(
                "IRW Khazara",
                100.0,
                100.0,
                100.0,
                100.0,
                2,
                2,
                new ArrayList<>(
                        Arrays.asList(
                                new Cargo("Borg-Schrott", 5),
                                new Cargo("Rote Materie", 2),
                                new Cargo("Plasma-Waffe", 50)
                        )
                )
        );

        Spaceship vulkanier = new Spaceship(
                "Ni'Var",
                80.0,
                80.0,
                50.0,
                100.0,
                0,
                5,
                new ArrayList<>(
                        Arrays.asList(
                                new Cargo("Forschungssonde", 35),
                                new Cargo("Photonentorpedo", 3)
                        )
                )
        );

        // Die Klingonen schießen mit dem Photonentorpedo einmal auf die Romulaner.
        klingonen.shootPhotonTorpedoes(romulaner);
        // Die Romulaner schießen mit der Phaserkanone zurück.
        romulaner.shootPhaserCanons(klingonen);
        // Die Vulkanier senden eine Nachricht an Alle “Gewalt ist nicht logisch”.
        vulkanier.notifyAll("Gewalt ist nicht logisch");
        // Die Klingonen rufen den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus. (Geschieht beides in printStatus())
        klingonen.printStatus();
        // Die Vulkanier sind sehr sicherheitsbewusst und setzen alle Androiden zur Aufwertung ihres Schiffes ein.
        vulkanier.sendRepairCommand(true, true, true, vulkanier.getAndroids());
        // Die Vulkanier verladen Ihre Ladung “Photonentorpedos” in die Torpedoröhren Ihres Raumschiffes und räumen das Ladungsverzeichnis auf.
        vulkanier.loadPhotonTorpedoesFromCargo(3);
        // Die Klingonen schießen mit zwei weiteren Photonentorpedo auf die Romulaner.
        klingonen.shootPhotonTorpedoes(romulaner);
        klingonen.shootPhotonTorpedoes(romulaner);
        // Die Klingonen, die Romulaner und die Vulkanier rufen jeweils den Zustand Ihres Raumschiffes ab und geben Ihr Ladungsverzeichnis aus.
        klingonen.printStatus();
        romulaner.printStatus();
        vulkanier.printStatus();
        // Geben Sie den broadcastKommunikator aus.
        Spaceship.printFullLog();
    }
}
