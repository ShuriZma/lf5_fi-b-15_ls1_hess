import java.util.ArrayList;
import java.util.Objects;

/**
 * Class {@link Spaceship}
 * @author Markus Heß / ShuriZma
 */
public class Spaceship {
    /** Name of the {@link Spaceship}. */
    private String name = "";
    /** Energy supply percentage of the {@link Spaceship}. */
    private double energySupply = 0.0;
    /** Shield percentage of the {@link Spaceship}. */
    private double shields = 0.0;
    /** Hull percentage of the {@link Spaceship}. */
    private double hull = 0.0;
    /** Life support system percentage of the {@link Spaceship}. */
    private double lifeSupportSystem = 0.0;
    /** Amount of photon torpedoes of the {@link Spaceship}. */
    private int photonTorpedoes = 0;
    /** Amount of androids on the {@link Spaceship}. */
    private int androids = 0;
    /** Cargo manifest of the {@link Spaceship} containing all loaded {@link Cargo}. */
    private final ArrayList<Cargo> cargoManifest = new ArrayList<>();
    /** Broadcast communicator shared between all {@link Spaceship}. */
    private static final ArrayList<String> broadcastCommunicator = new ArrayList<>();

    /** Default constructor */
    public Spaceship(){}

    /**
     * Full constructor
     * @param name {@link Spaceship#name}
     * @param energySupply {@link Spaceship#energySupply}
     * @param shields {@link Spaceship#shields}
     * @param hull {@link Spaceship#hull}
     * @param lifeSupportSystem {@link Spaceship#lifeSupportSystem}
     * @param photonTorpedoes {@link Spaceship#photonTorpedoes}
     * @param androids {@link Spaceship#androids}
     * @param cargos {@link Spaceship#cargoManifest}
     */
    public Spaceship(
            String name,
            double energySupply,
            double shields,
            double hull,
            double lifeSupportSystem,
            int photonTorpedoes,
            int androids,
            ArrayList<Cargo> cargos
    ) {
        this.name = name;
        this.energySupply = energySupply;
        this.shields = shields;
        this.hull = hull;
        this.lifeSupportSystem = lifeSupportSystem;
        this.photonTorpedoes = photonTorpedoes;
        this.androids = androids;
        this.cargoManifest.addAll(cargos);
    }

    /** @return {@link Spaceship#name} */
    public String getName() {
        return name;
    }

    /** @param name {@link Spaceship#name} */
    public void setName(String name) {
        this.name = name;
    }

    /** @return {@link Spaceship#energySupply} */
    public double getEnergySupply() {
        return energySupply;
    }

    /** @param energySupply {@link Spaceship#energySupply} */
    public void setEnergySupply(double energySupply) {
        this.energySupply = energySupply;
    }

    /** @return {@link Spaceship#shields} */
    public double getShields() {
        return shields;
    }

    /** @param shields {@link Spaceship#shields} */
    public void setShields(double shields) {
        this.shields = shields;
    }

    /** @return {@link Spaceship#hull} */
    public double getHull() {
        return hull;
    }

    /** @param hull {@link Spaceship#hull} */
    public void setHull(double hull) {
        this.hull = hull;
    }

    /** @return {@link Spaceship#lifeSupportSystem} */
    public double getLifeSupportSystem() {
        return lifeSupportSystem;
    }

    /** @param lifeSupportSystem {@link Spaceship#lifeSupportSystem} */
    public void setLifeSupportSystem(double lifeSupportSystem) {
        this.lifeSupportSystem = lifeSupportSystem;
    }

    /** @return {@link Spaceship#photonTorpedoes} */
    public int getPhotonTorpedoes() {
        return photonTorpedoes;
    }

    /** @param photonTorpedoes {@link Spaceship#photonTorpedoes} */
    public void setPhotonTorpedoes(int photonTorpedoes) {
        this.photonTorpedoes = photonTorpedoes;
    }

    /** @return {@link Spaceship#androids} */
    public int getAndroids() {
        return androids;
    }

    /** @param androids {@link Spaceship#androids} */
    public void setAndroids(int androids) {
        this.androids = androids;
    }

    /** @return {@link Spaceship#cargoManifest} */
    public ArrayList<Cargo> getCargoManifest() {
        return cargoManifest;
    }

    /** @param cargo {@link Cargo} that should be added to the {@link Spaceship#cargoManifest} */
    public void loadCargo(Cargo cargo) {
        this.cargoManifest.add(cargo);
    }

    /** @param target {@link Spaceship} that should be targeted. */
    public void shootPhotonTorpedoes(Spaceship target) {
        if (this.photonTorpedoes == 0) {
            System.out.print("Keine Photonentorpedos gefunden!\n\n");
            this.notifyAll("-=*Click*=-");
            return;
        }

        this.photonTorpedoes -= 1;
        this.notifyAll("Photonentorpedo abgeschossen");
        target.registerDamage();
    }

    /** @param target {@link Spaceship} that should be targeted. */
    public void shootPhaserCanons(Spaceship target) {
        if (this.energySupply < 50.0) {
            this.notifyAll("-=*Click*=-");
            return;
        }

        this.energySupply -= 50.0;
        this.notifyAll("Phaserkanone abgeschossen");
        target.registerDamage();
    }

    /** Checks for shields and reduces shield and hull percentage accordingly. */
    private void registerDamage() {
        System.out.print(this.name + " wurde getroffen!\n\n");
        if (this.shields > 0.0) {
            this.shields -= 50.0;
        }

        if (this.shields <= 0.0) {
            this.shields = 0.0;
            this.hull -= 50.0;
            this.energySupply -= 50.0;

            if (this.energySupply < 0.0) {
                this.energySupply = 0.0;
            }
        }

        if (this.hull <= 0.0) {
            this.hull = 0.0;
            this.lifeSupportSystem = 0.0;
            this.notifyAll("Lebenserhaltungssysteme wurden vernichtet.");
        }
    }

    /** @param message - Message that should be added to the log. */
    public void notifyAll(String message) {
        broadcastCommunicator.add("\n" + this.name + ": " + message);
    }

    /** Prints the status of the {@link Spaceship} */
    public void printStatus() {
        System.out.printf(
                "%s:\nEnergy supply: %.2f\nShields: %.2f\nHull: %.2f\nLife support system: %.2f\nPhoton torpedoes: %d\nAndroids: %d\n",
                this.name,
                this.energySupply,
                this.shields,
                this.hull,
                this.lifeSupportSystem,
                this.photonTorpedoes,
                this.androids
        );
        System.out.print("Cargo manifest:\n");
        this.cargoManifest.forEach(cargo -> System.out.print(cargo.toString()));
    }

    /** Prints all the contents of the {@link Spaceship#broadcastCommunicator} */
    public static void printFullLog() {
        System.out.printf("Logs:%s", broadcastCommunicator.toString().replaceAll("[\\[\\],]", "" ));
    }

    /** @param amountWanted - Amount of {@link Spaceship#photonTorpedoes} that should be added from loaded {@link Cargo} */
    public void loadPhotonTorpedoesFromCargo(int amountWanted) {
        int amountToAdd = 0;
        for (Cargo cargo : this.cargoManifest) {
            if (amountWanted < 1) {
                break;
            }
            if (Objects.equals(cargo.getName(), "Photonentorpedo")) {
                if (cargo.getAmount() < amountWanted) {
                    amountToAdd += cargo.getAmount();
                    amountWanted -= cargo.getAmount();
                    cargo.setAmount(0);
                } else {
                    amountToAdd += amountWanted;
                    cargo.setAmount(cargo.getAmount() - amountWanted);
                    amountWanted = 0;
                }
            }
        }


        this.photonTorpedoes += amountToAdd;
        System.out.print(amountToAdd + " Photonentorpedo(s) eingesetzt\n\n");

        this.cleanUpCargoManifest();
    }

    /**
     * @param repairShield - Should the {@link Spaceship#shields} be repaired?
     * @param repairEnergySupply - Should the {@link Spaceship#energySupply} be repaired?
     * @param repairHull - Should the {@link Spaceship#hull} be repaired?
     * @param amountAndroids {@link Spaceship#androids} that should be used to repair.
     */
    public void sendRepairCommand(boolean repairShield, boolean repairEnergySupply, boolean repairHull, int amountAndroids) {
        int randomInteger = (int) (Math.random() * 100);
        int structuresToBeRepaired = 0;
        if (repairShield) {
            structuresToBeRepaired += 1;
        }

        if (repairEnergySupply) {
            structuresToBeRepaired += 1;
        }

        if (repairHull) {
            structuresToBeRepaired += 1;
        }

        if (repairShield) {
            this.shields += (double) (randomInteger * Math.min(amountAndroids, this.androids)) / structuresToBeRepaired;
        }

        if (repairEnergySupply) {
            this.energySupply += (double) (randomInteger * Math.min(amountAndroids, this.androids)) / structuresToBeRepaired;
        }

        if (repairHull) {
            this.hull += (double) (randomInteger * Math.min(amountAndroids, this.androids)) / structuresToBeRepaired;
        }
    }

    /** Prints all {@link Cargo} from {@link Spaceship#cargoManifest}. */
    public void printCargo() {
        this.cargoManifest.forEach(cargo -> System.out.print(cargo.toString()));
    }

    /** Purges empty {@link Cargo} from {@link Spaceship#cargoManifest}. */
    public void cleanUpCargoManifest() {
        this.cargoManifest.removeIf(cargo -> cargo.getAmount() <= 0);
    }
}
