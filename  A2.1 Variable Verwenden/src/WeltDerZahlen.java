/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie d�rfen nicht die Namen der Variablen ver�ndern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Markus He� >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
	  byte anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstra�e
	  long anzahlSterne = 200000000000L;
    
    // Wie viele Einwohner hat Berlin?
      int bewohnerBerlin = 3688976;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
      short alterTage = 7502;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
      int gewichtKilogramm = 190000;
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
      int flaecheGroesstesLand = 17098242;
    
    // Wie gro� ist das kleinste Land der Erde?
    
      float flaecheKleinstesLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne: " + anzahlSterne);
    
    System.out.println("Anzahl der Einwohner Berlins: " + bewohnerBerlin);
    
    System.out.println("Alter in Tage: " + alterTage);
    
    System.out.println("Gewicht des schwersten Tiers der Welt: " + gewichtKilogramm + "kg");

    System.out.println("Fl�che des gr��ten Landes der Welt: " + flaecheGroessteLand + "km�");
    
    System.out.println("Fl�che des kleinsten Landes der Welt: " + flaecheKleinsteLand + "km�");
    
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
