public class Modulo {
    public static void main(String[] args) {
        System.out.print("Die folgenden Zahlen sind durch 4 und 7, aber nicht durch 5 teilbar:");
        for(int i = 1; i <= 200; i++) {
            if(i % 7 == 0 && i % 5 != 0 && i % 4 == 0) {
                System.out.printf(" %d", i);
            }
        }
    }
}
