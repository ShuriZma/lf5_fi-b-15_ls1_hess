public class MultiplicationTable {
    public static void main(String[] args) {
        int row = 0;
        int column;
        while (row < 10) {
            row++;
            column = 0;
            while (column < 10) {
                column++;
                if(row * column < 100) {
                    if(row * column < 10) {
                        System.out.print("   " + row * column);
                    } else {
                        System.out.print("  " + row * column);
                    }
                } else {
                    System.out.print(" " + row * column);
                }
            }
            System.out.println();
        }
    }
}
