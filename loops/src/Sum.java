import java.util.Scanner;

public class Sum {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Geben Sie eine Zahl an: ");
        int number = input.nextInt();

        int sum = 0;
        if(number > 1) {
            for (int currentNumber = 0; currentNumber <= number; currentNumber++) {
                sum += currentNumber;
            }
            System.out.printf("Die Summe für A beträgt: %d\n", sum);

            sum = 0;

            for (int currentNumber = 0; currentNumber <= number; currentNumber++) {
                sum += 2 * currentNumber;
            }
            System.out.printf("Die Summe für B beträgt: %d\n", sum);

            sum = 0;

            for (int currentNumber = 0; currentNumber <= number; currentNumber++) {
                sum += 2 * currentNumber + 1;
            }
            System.out.printf("Die Summe für C beträgt: %d\n", sum);
        } else {
            for (int currentNumber = 0; number <= currentNumber; currentNumber--) {
                sum += currentNumber;
            }
            System.out.printf("Die Summe für A beträgt: %d\n", sum);

            sum = 0;

            for (int currentNumber = 0; number <= currentNumber; currentNumber--) {
                sum += 2 * currentNumber;
            }
            System.out.printf("Die Summe für B beträgt: %d\n", sum);

            sum = 0;

            for (int currentNumber = 0; number <= currentNumber; currentNumber--) {
                sum += 2 * currentNumber + 1;
            }
            System.out.printf("Die Summe für C beträgt: %d\n", sum);
        }
    }
}
