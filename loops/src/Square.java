import java.util.Scanner;

public class Square {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Gib die Seitenlänge des Quadrats an: ");
        int length = input.nextInt();

        for (int row = 0; row < length; row++) {
            String string = "";
            if (row == 0 || row == length - 1) {
                for (int i = 0; i < length; i++) {
                    string += "*  ";
                }
            } else {
                for (int i = 0; i < length; i++) {
                    if (i == 0 || i == length - 1) {
                        string += "*  ";
                    } else {
                        string += "   ";
                    }
                }
            }
            System.out.println(string);
        }
    }
}
