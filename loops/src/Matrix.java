import java.util.Scanner;

import static java.lang.String.*;

public class Matrix {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int row = 0;
        int column;

        System.out.println("Gib eine Zahl zwischen 2 und 9 an: ");
        int number = input.nextInt();

        if (number > 9 || number < 2) {
            number = 1;
        }

        int[] numbers = {
                9000, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99
        };

        int run = 0;
        while (row < 10) {
            row++;
            column = 0;
            while (column < 10) {
                run ++;
                column++;
                int currentNumber = run;

                if(numbers[currentNumber] == 0) {
                    System.out.print(" 0 ");
                    continue;
                }

                if(numbers[currentNumber] % number == 0 || Matrix.contains(numbers[currentNumber], number) || Matrix.crossSum(numbers[currentNumber], number)) {
                    System.out.print(" * ");
                } else {
                    if(numbers[currentNumber] < 10) {
                        System.out.printf(" %d ", numbers[currentNumber]);
                    } else {
                        System.out.printf("%d ", numbers[currentNumber]);
                    }
                }
            }
            System.out.println();
        }
    }

    private static boolean crossSum(int sum, int number) {
        return valueOf(sum).chars().map(Character::getNumericValue).sum() == number;
    }

    private static boolean contains(int currentNumber, int number) {
        while(currentNumber > 0)
        {
            if(currentNumber % 10 == number)
                return true;

            currentNumber = currentNumber/10;
        }
        return false;
    }
}
