import java.util.Scanner;

public class Stairs {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Gib die Treppenhöhe an: ");
        int height = input.nextInt();
        System.out.print("\nGib die Stufenweite an: ");
        int width = input.nextInt();
        int totalWidth = height * width;

        for(int i = 1; i <= height; i++) {
            for(int j = 0; j < totalWidth - i * width; j++) {
                System.out.print(" ");
            }
            for(int k = 0; k < i * width; k++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }
}
