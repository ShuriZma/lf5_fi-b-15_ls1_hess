import java.util.Scanner;

public class Count {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Geben Sie eine Zahl an: ");
        int startNumber = input.nextInt();
        int number = 0;

        if(startNumber > 1) {
            System.out.printf("Zählen von 1 bis %d\n", startNumber);
            for (int i = 0; i < startNumber; i++) {
                number ++;
                System.out.printf("%d\n", number);
            }
            number ++;
            System.out.printf("Zählen von %d bis 1\n", startNumber);
            for (int i = startNumber; i > 0; i--) {
                number --;
                System.out.printf("%d\n", number);
            }
        } else {
            System.out.printf("Zählen von 1 bis %d\n", startNumber);
            for (int i = 1; i >= startNumber; i--) {
                number --;
                System.out.printf("%d\n", number);
            }
            number --;
            System.out.printf("Zählen von %d bis 1\n", startNumber);
            for (int i = startNumber; i < 2; i++) {
                number ++;
                System.out.printf("%d\n", number);
            }
        }
    }
}
