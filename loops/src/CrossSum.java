import java.util.Scanner;

public class CrossSum {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int sum = 0;

        System.out.print("Geben sie bitte eine Zahl ein: ");
        int num = input.nextInt();

        do {
            sum += num % 10;
            num /= 10;
        } while (num > 0);

        System.out.println(sum);
    }
}
