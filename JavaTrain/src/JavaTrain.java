import java.util.Scanner;

public class JavaTrain {

	public static void main(String[] args) {

		char[] inputs = getInput();
		char haltInSpandau = inputs[0];
		char richtungHamburg = inputs[1];
		char haltInStendal = inputs[2];
		char endetIn  = inputs[3];

		int fahrzeit = getFahrzeit(haltInSpandau, richtungHamburg, haltInStendal, endetIn);
		String endetInFull = getEndetIn(richtungHamburg, endetIn);

		printResult(fahrzeit, endetInFull);
	}

	private static char[] getInput() {
		Scanner input = new Scanner(System.in);

		System.out.print("Hält der Zug in Spandau? (j/n)");
		char haltInSpandau = input.next().charAt(0);
		System.out.print("Fährt der Zug Richtung Hamburg? (j/n)");
		char richtungHamburg = input.next().charAt(0);
		if(richtungHamburg == 'j') {
			return new char[]{haltInSpandau, richtungHamburg, 'n', 'n'};
		}
		System.out.print("Hält der Zug in Stendal? (j/n)");
		char haltInStendal = input.next().charAt(0);
		System.out.print("Wo endet der Zug? (Braunschweig = b/Hannover = h/Wolfsburg = w)");
		char endetIn = input.next().charAt(0);

		return new char[]{haltInSpandau, richtungHamburg, haltInStendal, endetIn};
	}
	private static void printResult(int fahrzeit, String endetInFull) {
		if (endetInFull.equals("Hogwarts")) {
			System.out.printf("Der Zug endet in %s. Die Fahrtzeit beträgt 9 Stunden", endetInFull);
		} else {
			System.out.printf("Der Zug endet in %s. Die Fahrtzeit beträgt %d Minuten", endetInFull, fahrzeit);
		}
	}

	private static String getEndetIn(char richtungHamburg, char endetIn) {
		String endetInFull;
		if (endetIn == 'w') {
			endetInFull = "Wolfsburg"; // Endet in Wolfsburg
		} else if (endetIn == 'b') {
			endetInFull = "Braunschweig"; // Endet in Braunschweig
		} else if (endetIn == 'h') {
			endetInFull = "Hannover"; // Endet in Hannover
		} else if (richtungHamburg == 'j') {
			endetInFull = "Hamburg";
		} else {
			endetInFull = "Hogwarts";
		}
		return endetInFull;
	}

	private static int getFahrzeit(char haltInSpandau, char richtungHamburg, char haltInStendal, char endetIn) {
		int fahrzeit = 0;
		fahrzeit = fahrzeit + 8; // Fahrzeit: Berlin Hbf -> Spandau

		if (haltInSpandau == 'j') {
			fahrzeit = fahrzeit + 2; // Halt in Spandau
		}

		if (richtungHamburg == 'j') {
			fahrzeit = fahrzeit + 96; // Endet in Hamburg
		} else {
			fahrzeit = fahrzeit + 34;

			if (haltInStendal == 'j') {
				fahrzeit = fahrzeit + 16; // Halt in Stendal
			} else {
				fahrzeit = fahrzeit + 6; // Kein Halt in Stendal
			}

			if (endetIn == 'w') {
				fahrzeit = fahrzeit + 29; // Endet in Wolfsburg
			} else if (endetIn == 'b') {
				fahrzeit = fahrzeit + 50; // Endet in Braunschweig
			} else if (endetIn == 'h') {
				fahrzeit = fahrzeit + 62; // Endet in Hannover
			}
		}
		return fahrzeit;
	}
}
