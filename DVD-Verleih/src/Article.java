import java.util.ArrayList;

public class Article {
    private String article = "";
    private int number = 0;
    private double purchasePrice = 0.0;
    private double sellPrice = 0.0;
    private int minStock = 0;
    private int stock = 0;

    public Article() {
    }

    public Article(
            String article,
            int number,
            double purchasePrice,
            double sellPrice,
            int minStock,
            int stock
    ) {
        this.article = article;
        this.number = number;
        this.purchasePrice = purchasePrice;
        this.sellPrice = sellPrice;
        this.minStock = minStock;
        this.stock = stock;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public double getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(double purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public double getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(double sellPrice) {
        this.sellPrice = sellPrice;
    }

    public int getMinStock() {
        return minStock;
    }

    public void setMinStock(int minStock) {
        this.minStock = minStock;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public boolean orderArticle() {
        boolean shouldReorder = this.stock * 100 / this.minStock < 80;
        if (!shouldReorder) {
            return false;
        }

        this.stock = this.minStock;
        return true;
    }

    public void calcProfit() {
        double profit = this.sellPrice - this.purchasePrice;

        System.out.printf("Profit: %.2f€\n", profit);
    }
}
