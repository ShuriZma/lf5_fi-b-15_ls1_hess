import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Article> articles = new ArrayList<>();
        String[] names = {"Phone", "Dinner", "Restaurant", "Professor", "Role", "Government", "Profession", "Performance", "Medicine", "Guest", "Meaning", "Database", "Session", "Agency", "Marketing", "Replacement", "Insect", "Technology", "Diamond", "Contract"};
        while (articles.size() < 10) {
            articles.add(new Article(
                    names[(int) (Math.random() * names.length)],
                    (int)Math.floor(Math.random() * 10),
                    Math.random() * 10,
                    Math.random() * 10,
                    (int)Math.floor(Math.random() * 10) + 10,
                    (int)Math.floor(Math.random() * 10)
            ));
        }

        articles.forEach(article -> {
            System.out.printf(
                    "Name: %s, article number: %d, purchase price: %.2f€, sell price: %.2f€, minimum stock: %d, stock: %d\n",
                    article.getArticle(),
                    article.getNumber(),
                    article.getPurchasePrice(),
                    article.getSellPrice(),
                    article.getMinStock(),
                    article.getStock()
            );

            if(article.orderArticle()) {
                System.out.printf("New Stock after restocking: %d\n", article.getStock());
            }

            article.calcProfit();
            System.out.println();
        });
    }
}
