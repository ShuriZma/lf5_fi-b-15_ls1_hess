
public class Aufgabe1 {

	public static void main(String[] args) {
		// Der Unterschied zwischen print() und println() ist, dass println() automatisch mit einem Zeilenumbruch endet(!).
		// Genau wie print() beginnt println() nicht(!) auf einer neuen Zeile.
		String lang = "JavaScript";
		System.out.print(lang + " ist definitiv eine \"tolle\" Sprache.\r\nNicht.");
		System.out.print("\r\nDas war offensichtlich ein Witz.\r\n" + lang + " ist eine super Sprache!");
		
		System.out.print("\r\n\r\nAufgabe 2\r\n\r\n");
		
		System.out.print("\r\n      *\r\n" + "     ***\r\n" + "    *****\r\n" + "   *******\r\n" + "  *********\r\n" + " ***********\r\n" + "*************\r\n" + "     ***\r\n" + "     ***");

		System.out.print("\r\n\r\nAufgabe 3\r\n\r\n");
		
		double a = 22.4234234;
		double b = 111.2222;
		double c = 4.0;
		double d = 1000000.551;
		double e = 97.34;
		
		System.out.printf("%.2f \r\n", a);
		System.out.printf("%.2f \r\n", b);
		System.out.printf("%.2f \r\n", c);
		System.out.printf("%.2f \r\n", d);
		System.out.printf("%.2f \r\n", e);
	}

}
