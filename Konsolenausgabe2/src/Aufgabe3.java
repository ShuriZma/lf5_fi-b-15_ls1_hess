
public class Aufgabe3 {

	public static void main(String[] args) {
		System.out.printf("%-12s|%10s", "Fahrenheit", "Celsius");
		System.out.print("\r\n");
		System.out.print("-----------------------");
		System.out.print("\r\n");
		System.out.printf("%-12d|%10.2f", -20, -28.8889);
		System.out.print("\r\n");
		System.out.printf("%-12d|%10.2f", -10, -23.3333);
		System.out.print("\r\n");
		System.out.printf("+%-11d|%10.2f", 0, -17.7778);
		System.out.print("\r\n");
		System.out.printf("+%-11d|%10.2f", 20, -6.6667);
		System.out.print("\r\n");
		System.out.printf("+%-11d|%10.2f", 30, -1.1111);
	}

}
