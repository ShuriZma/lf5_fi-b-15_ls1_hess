
public class Aufgabe2 {

	public static void main(String[] args) {
		System.out.printf("%-5s = %-19s = %4d", "0!", "" ,1);
		System.out.print("\r\n");
		System.out.printf("%-5s = %-19d = %4d", "1!", 1 ,1);
		System.out.print("\r\n");
		System.out.printf("%-5s = %-19s = %4d", "2!", "1 * 2" ,2);
		System.out.print("\r\n");
		System.out.printf("%-5s = %-19s = %4d", "3!", "1 * 2 * 3" ,6);
		System.out.print("\r\n");
		System.out.printf("%-5s = %-19s = %4d", "4!", "1 * 2 * 3 * 4" ,24);
		System.out.print("\r\n");
		System.out.printf("%-5s = %-19s = %4d", "4!", "1 * 2 * 3 * 4 * 5" ,120);

	}

}
